# Loxon PUML library

![Project status](https://img.shields.io/badge/project%20status-not%20ready-red)

This is a work-in-progress toolkit to help PUML - based documentation in loxon.

- [PUML for dummies](documentation/puml-for-dummies.md)
- [Quick start](documentation/quick-start.md)
- [What is loxon library?](documentation/loxon-library.md)
- [Example of usage](examples/examples.puml)
