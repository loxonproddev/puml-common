# Quick start guide

For rendering of PUML files there are plugins for IntelliJ or VSCode.

Practically, if you start editing .PUML file in IntelliJ it will recommend the necessary plugin.

The rendering will work only if you installed GraphViz software on your computer.  
