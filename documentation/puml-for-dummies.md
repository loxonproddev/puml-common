# PUML for dummies

### Contents

- [Syntax](#Syntax)
- [Sprite generation](#Sprite generation)

### Syntax

PUML is a syntax which can define UML diagrams and also a tool which can render such definition files.

The most basic diagram is something like this:

```puml
@startuml
rectangle "Hello world"
@enduml
```

`@startuml` and `@enduml` must be also at the beginning and ending of your file.

There are some basic built-in shapes which are a bit more complex than a simple rectangle, but the good thing that you can also define your own shapes, like:

```puml
!define TECHN_FONT_SIZE 12
!definelong LoxonEntity(e_alias, e_label, e_techn, e_descr, e_color, e_sprite)
rectangle "==e_label\n<color:e_color><$e_sprite></color>\n//<size:TECHN_FONT_SIZE>[e_techn]</size>//" as e_alias
```

`!define` and `!definelong` are basically the same thing, but in case of the latter the definition can be multi-lined.
As you can see PUML has some HTML-like notation so you can show some style.

Your own shape-definitions can have also inheritance - relations between each other:

```puml
!definelong LoxonServerLib(alias, label, descr)
LoxonEntity(alias, label, "Server lib", descr, "WhiteSmoke", i_server)
!enddefinelong
```

You can also make some styling:

```puml
skinparam rectangle {
    StereotypeFontColor FONT_COLOR
    FontColor FONT_COLOR
    BackgroundColor BACKGROUND_COLOR
    BorderColor BORDER_COLOR
    shadowing false
}
```

### Sprite generation

Sprites are little icons, defined in a strange format (examples in [icons file](../source/icons.puml)) You can generate such spites:

1. Download [plantuml.jar](http://plantuml.com/download)
2. The icon should have png format and **white** background (instead of transparent), because the white color is considered as transparent by plantuml `¯\_(ツ)_/¯`
3. `java -jar plantuml.jar -encodesprite 8 foo.png > foo.sprite`

Also, definition files can be included, also with url.
See more information [on the official site](http://plantuml.com)!
